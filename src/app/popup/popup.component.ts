import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-popup',
  template: `
    <p>
      popup works!
    </p>
    <div *ngFor="let msg of messages"><code>{{msg}}</code></div>
  `,
  styles: [`
    div {
      padding: .5rem;
      background-color: #dbdbdb;
      margin: .5rem 0;
    }
  `]
})
export class PopupComponent implements OnInit {

  messages: string[] = [];
  
  constructor() { }

  ngOnInit() {
    console.log('Popup init');

    addEventListener('message', (event) => {
      if (event.data.type === 'testEvent' && opener && event.origin === (<Window>opener).location.origin) {
        console.log('Received message', event.data);
        this.messages.push(event.data.message);
      }
    });
    
    // Close the popup when the opener window is closed or navigates away from the app.
    opener.addEventListener('unload', (event) => {
      this.messages.push('Opener closed, popup closing in 2 seconds.')
      setTimeout(() => {
        close();
      }, 2000);
    });

    postMessage({type: 'popupEvent', message: 'ready'}, location.origin);
  }
}
