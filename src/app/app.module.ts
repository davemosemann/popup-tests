import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { OtherParentComponent } from './other-parent/other-parent.component';
import { PopupService } from './popup.service';
import { PopupComponent } from './popup/popup.component';
import { CommonModule } from '@angular/common';
import { ParentShellComponent } from './parent-shell/parent-shell.component';


@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    ChildComponent,
    OtherParentComponent,
    PopupComponent,
    ParentShellComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: 'parent',
        component: ParentShellComponent,
        children: [
          {
            path: '',
            component: ParentComponent
          },
          {
            path:'child',
            component: ChildComponent
          },
        ]
      },
      {
        path: 'other-parent',
        component: OtherParentComponent
      },
      {
        path: 'popup',
        component: PopupComponent
      },
      {
        path: '',
        redirectTo: '/parent', 
        pathMatch: 'full'
      }
    ])
  ],
  providers: [PopupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
