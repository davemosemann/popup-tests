import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  template: `
    <p>
      parent works!
    </p>
    <a routerLink="../other-parent">Other Parent</a>
    <a routerLink="child">Child</a>
  `,
  styles: []
})
export class ParentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
