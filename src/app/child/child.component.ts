import { Component, OnInit } from '@angular/core';
import { PopupService } from '../popup.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-child',
  template: `
    <p>
      child works!
    </p>
    <a routerLink="../">Parent</a>
    <button (click)="openPopup()">Open Popup</button>
    <button (click)="sendMessage()">Send Message</button>
  `,
  styles: []
})
export class ChildComponent implements OnInit, OnDestroy {

  private popup?: Window;

  constructor(private popupService: PopupService) { }

  ngOnInit() {
    if (this.popupService.popups['test']) {
      this.popup = this.popupService.popups['test'];
      this.sendMessage('Existing popup found');
    } else {
      this.openPopup();
    }
  }

  ngOnDestroy() {
    this.popup.postMessage({type: 'testEvent', message: 'ChildComponent destroyed'}, location.origin);
  }

  async openPopup(): Promise<void> {
    this.popup = await this.popupService.openPopup(`${location.origin}/popup`, 'test');
    this.popupService.cachePopup(this.popup, 'test');
    this.sendMessage('Popup opened');
  }
  
  sendMessage(message?: string): void {
    if (this.popup) {
      this.popup.postMessage({type: 'testEvent', message: message || 'test message'}, window.location.origin);
    }
  }

}
