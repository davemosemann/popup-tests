import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-other-parent',
  template: `
    <p>
      other-parent works!
    </p>
    <a routerLink="../parent">Parent</a>
  `,
  styles: []
})
export class OtherParentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
