import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { PopupService } from '../popup.service';

@Component({
  template: `
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class ParentShellComponent implements OnDestroy {

  constructor(private popupService: PopupService) { }

  ngOnDestroy(): void {
    if (this.popupService.popups['test']) {
      this.popupService.popups['test'].close();
    }
  }

}
