import { Injectable } from '@angular/core';

@Injectable()
export class PopupService {

  public popups: {[name: string]: Window} = {};

  constructor() { }

  openPopup(path: string, name: string): Promise<Window> | Window {
    return new Promise<Window>((resolve, reject) => {
      const popup: Window = window.open(path, name, 'width=400,height=400');

      // Listen for the message we send from ngOnInit so we can start sending messages
      popup.addEventListener('message', (event) => {
        if (event.data.type === 'popupEvent' && event.data.message === 'ready') {
          resolve(popup);
        } 
      });
    });
  }

  /** Store the popup to reference later, delete it when it is closed */
  cachePopup(popup: Window, name: string): void {
    this.popups[name] = popup;
    popup.addEventListener('unload', () => {
      delete this.popups[name];
    });
  }

}